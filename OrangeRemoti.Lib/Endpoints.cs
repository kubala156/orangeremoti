﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrangeRemoti.Lib
{
    public class Endpoints
    {
        

        /// <summary>
        /// Device info endpoint
        /// </summary>
        public const string DEVICE_INFO = "/remoteControl/cmd?operation=10";

        /// <summary>
        /// Get endpoint for command execution
        /// </summary>
        /// <param name="code"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static string GetCommandEndpoint(int code, int mode) => $"/remoteControl/cmd?operation=01&key={code}&mode={mode}";
    }
}
